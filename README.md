# dotfiles

list of files in this repo:

- .config/Thunar
- .config/qBittorrent
- .config/smplayer
- .config/sublime-text-3
- .config/xfce4
- .i3/*
- .mpd/*
- .mplayer/*
- .ncmpcpp/*
- .Xresources
- .compton.conf
- .zshrc
- README.md
- updated screenfetch

Configs and dotfiles

- OS: Arch Linux (using Architect OS)
- Terminal: zsh + oh my zsh + rxvt-unicode
- WM/DE: i3-gaps (AUR) / XFCE4
- File Manager: Thunar
- Text Editor: Sublime Text 3 / nano
- Video: mplayer+smplayer
- Torrents: qBittorrent
- Compiste Manager: compton

# Installing Arch Linux quickly

[Architect Linux](http://sourceforge.net/projects/architect-linux/)

# Screenshot

![screenshot](screenFetch-2015-12-14_14-17-32.png)